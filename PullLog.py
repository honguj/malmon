__author__ = 'hoangu'

import time
import sys

def tail_f(file):
    interval = 1.0

    while True:
        where = file.tell()
        line = file.readline()
        if not line:
          time.sleep(interval)
          file.seek(where)
        else:
          yield line

for line in tail_f(open(sys.argv[1])):
    print line,