# Get project name
$projectName = ((Get-Content .\Config.txt)[0] -split ":")[1]
write-host "Project name: $projectName"

$currentPath = (Get-Item -Path ".\" -Verbose).FullName

#Creating a new event filter
$instanceFilter = ([wmiclass]"\\.\root\subscription:__EventFilter").CreateInstance()

$instanceFilter.QueryLanguage = "WQL"
$instanceFilter.Query = "select * from __InstanceCreationEvent within 5 where targetInstance isa 'win32_Process'"
$instanceFilter.Name = $projectName + "Filter"
$instanceFilter.EventNamespace = 'root\cimv2'

$result = $instanceFilter.Put()
$newFilter = $result.Path
write-host "Creating __EventFilter: $instanceFilter.Name - Path: $newFilter" -foreground "Green"

#Creating a new event consumer
$instanceConsumer = ([wmiclass]"\\.\root\subscription:LogFileEventConsumer").CreateInstance()

$instanceConsumer.Name = $projectName + 'Consumer'
$instanceConsumer.Filename = $currentPath + "\Log.log"
#$instanceConsumer.Text = "PID: %TargetInstance.ProcessId% - %TargetInstance.Name% created"
$instanceConsumer.Text = "PID %TargetInstance.ProcessId%: %TargetInstance.Name% created (%TargetInstance.CommandLine%)"

$result = $instanceConsumer.Put()
$newConsumer = $result.Path
write-host "Creating LogFileEventConsumer: $instanceConsumer.Name - Path: $newConsumer" -foreground "Green"

#Bind filter and consumer
$instanceBinding = ([wmiclass]"\\.\root\subscription:__FilterToConsumerBinding").CreateInstance()

$instanceBinding.Filter = $newFilter
$instanceBinding.Consumer = $newConsumer
$result = $instanceBinding.Put()
$newBinding = $result.Path
write-host "Creating __FilterToConsumerBinding - Path: $newBinding" -foreground "Green"

##Removing WMI Subscriptions using [wmi] and Delete() Method
##([wmi]$newFilter).Delete()
##([wmi]$newConsumer).Delete()
##([wmi]$newBinding).Delete()