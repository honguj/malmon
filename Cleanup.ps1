# Get project name
$projectName = ((Get-Content .\Config.txt)[0] -split ":")[1]

# Iterate through all classes to look for wmi object related to the project
$classes = gwmi -namespace 'root\subscription' -list 
Foreach($class in $classes){
	$instances += gwmi -namespace 'root\subscription' -class $class.Name | ?  {$_.__Path -match $projectName} 
}

# Remove duplicates
$instances = $instances | sort -unique
$l = $instances.length
write-host "Found $l instances. Preparing to remove all of them" -foreground "Green"

# Removing all wmi objects related to the project
Foreach($path in $instances){
	write-host "- Removing $path.Path"
	([wmi]$path).Delete()
}

